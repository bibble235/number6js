const { check } = require('express-validator');

module.exports = {

  customValidatorUsername: check('username')

    // To delete leading and triling space
    .trim()

    // Validate contains bibble (doing it via custom)
    .contains('fred').withMessage('email must contain fred')

    // Custom validation
    .custom(async (username) => {
      if (!username.includes('bibble')) {
        throw new Error('username should contain bibble');
      }
    }),
};
