const dotenv = require('dotenv');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config();
}

const path = require('path');
const debug = require('debug')('app:appLocalStrategy');

const fs = require('fs');
const http = require('http');
const https = require('https');

const expressConfig = require('./config/expressConfig');

const app = expressConfig(path.join(__dirname, '/..'));

// Listen HTTP
const httpPort = process.env.HTTP_PORT || 4015;
const httpServer = http.createServer(app);
app.serverHttp = httpServer.listen(httpPort,
  () => debug(`HTTP Server listening of port ${httpPort}`));

// Listen HTTPS
const credentials = {
  key: fs.readFileSync(process.env.KEY_PEM),
  cert: fs.readFileSync(process.env.CERT_PEM),
};

const httpsServer = https.createServer(credentials, app);
const httpsPort = process.env.HTTPS_PORT || 4016;
app.serverHttps = httpsServer.listen(httpsPort,
  () => debug(`HTTPS Server listening of port ${httpsPort}`));

module.exports = app;
