const request = require('supertest');
const cheerio = require('cheerio');
require('should');

const debug = require('debug')('app:test');
const app = require('../index');

const agent = request.agent(app);

function extractCsrfToken(res) {
  const $ = cheerio.load(res.text);
  return $('[name=_csrf]').val();
}

after(async () => {
  debug('Calling end app for Http');
  app.serverHttp.close();

  debug('Calling end app for Http');
  app.serverHttps.close();

  debug('After complete');
});

describe('Home', async () => {
  it('should return have a csrf token', async () => {
    const res = await agent.get('/home').expect(200);

    const csrfToken = extractCsrfToken(res);
    csrfToken.should.not.be.empty();
  });
});
