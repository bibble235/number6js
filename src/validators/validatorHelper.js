class ValidatorHelper {
  static getStateForParam(errors, param) {
    const thisError = errors.find((error) => error.param === param);
    return thisError ? ' is-invalid ' : ' is-valid ';
  }

  static getFeedbackTextForParam(errors, param) {
    const thisError = errors.find((error) => error.param === param);
    const returnError = thisError ? thisError.msg : null;
    return returnError;
  }
}

module.exports = ValidatorHelper;
