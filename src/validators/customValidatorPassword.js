const { check } = require('express-validator');

module.exports = {

  customValidatorPassword: check('password')

    // To delete leading and triling space
    .trim()

    // Validate contains bibble (doing it via custom)
    .contains('x').withMessage('Password must contain x'),
};
