function appPassportConfig(repository, passport) {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((userId, done) => {
    repository.findUserById(userId, (err, user) => {
      if (err) { return done(err); }
      return done(false, user);
    });
  });
}

module.exports = (repository, passport) => appPassportConfig(repository, passport);
