const { check } = require('express-validator');

const { customValidatorUsername } = require('./customValidatorUsername');
const { customValidatorPassword } = require('./customValidatorPassword');

const testValidator = {
  loginValidator: [
    check('username').notEmpty().withMessage('Please provide a username'),
    customValidatorUsername,
    check('password').notEmpty().withMessage('Please provide a password'),
    customValidatorPassword,
  ],
};

module.exports = testValidator;
