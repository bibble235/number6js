const express = require('express');
const testController = require('../controller/testController');
const testValidator = require('../validators/testValidator');
const isLoggedIn = require('../authentication/authenticate');

function routes(appPassport) {
  const router = express.Router();
  const controller = testController(appPassport);

  router.get('/', controller.performHomeGet);
  router.get('/home', controller.performHomeGet);
  router.get('/public', controller.performPublicGet);
  router.get('/protected', isLoggedIn, controller.performProtectedGet);
  router.get('/login', controller.performLoginGet);
  router.get('/logout', controller.performLogoutGet);

  router.post('/home', controller.performHomePost);
  router.post('/public', controller.performPublicPost);
  router.post('/protected', controller.performProtectedPost);
  router.post('/login', testValidator.loginValidator, controller.performLoginPost);

  return router;
}

module.exports = routes;
