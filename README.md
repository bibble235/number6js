[![Express][express-url]][express-url]
[![NodeJs][nodejs-url]][nodejs-url]

# number6js

## Contents

- [number6js](#number6js)
  - [Contents](#contents)
  - [Description](#description)
  - [Features](#features)
  - [Change log](#change-log)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Run](#run)
  - [Tests](#tests)
  - [Contact](#contact)
  - [License](#license)

<!-- DESCRIPTION -->
## Description
This is an example app using Express.js.

<!-- FEATURES -->
## Features
- eslint setup https://eslint.org/
- ejs https://ejs.co/ 
- express 4.x https://expressjs.com/
- debug https://github.com/visionmedia/debug
- passport (LocalStrategy only) http://www.passportjs.org/
- bootstrap 5.x https://getbootstrap.com/

<!-- CHANGELOG -->
## Change log
- Added HTTPS
- Added Helmet
- Added csrf protection

<!-- PREREQUISITES -->
## Prerequisites
This example was test using the following set up
- Ubuntu 21.04 (hursute)
- Node v14.16.1
- Chrome 90.0.4430.93 

There is nothing specific which would prevent usage on other platforms, however it has only be testing using the above specification

<!-- INSTALLATION -->
## Installation

1.  Clone repository

    * git clone https://gitlab.com/bibble235/number6js


<!-- RUN -->
## Run
```sh
npm start
```

<!-- TEST -->
## Tests
There is no tests implemented at this time
```sh
npm test
```

<!-- CONTACT -->
## Contact
Iain (Bill) Wiseman - bw@bibble.co.nz

Project Link: [https://gitlab.com/bibble235/number6js](https://gitlab.com/bibble235/number6js)

<!-- LICENSE -->
## License
Distributed under the MIT License. See `LICENSE` for more information.

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->


[license-url]: https://img.shields.io/badge/License-MIT-blue.svg
[express-url]: https://img.shields.io/badge/Express.js-404D59?style=for-the-badge
[nodejs-url]:https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white