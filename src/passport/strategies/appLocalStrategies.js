const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

function appLocalStrategy(repository) {
  return new LocalStrategy(
    (username, password, done) => {
      repository.findUserByUsername({ username }, (err, user) => {
        if (err) { return done(err); }
        if (!user) { return done(null, false, { message: 'Incorrect username and/or password.' }); }
        return bcrypt.compare(password, user.password, (bcryptErr, authenticated) => {
          if (bcryptErr) { return done(bcryptErr, false, { message: 'Incorrect username and/or password.' }); }
          if (!authenticated) { return done(bcryptErr, false, { message: 'Incorrect username and/or password.' }); }
          return done(false, user);
        });
      });
    },
  );
}

module.exports = (repository) => appLocalStrategy(repository);
