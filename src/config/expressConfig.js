const express = require('express');
const helmet = require('helmet');

const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const csrf = require('csurf');

const path = require('path');
const flash = require('connect-flash');

const testRouter = require('../routes/testRouter');
const staticRoutesConfig = require('./staticRoutesConfig');

const appLocalStrategy = require('../passport/strategies/appLocalStrategies');
const appPassportConfig = require('../passport/passportConfig');
const passportFlash = require('../middleware/passportFlash');
const userInViews = require('../middleware/userInViews');
const appHelper = require('../middleware/appHelper');

const Repository = require('../data/repository/repository');

function expressConfig(baseDirectory) {
  // Create express app
  const expressInstance = express();

  // Required to parse body data
  expressInstance.use(express.json());

  // Required to parse incoming requests with urlencoded payloads
  expressInstance.use(express.urlencoded({ extended: true }));

  // Required to parse cookies
  expressInstance.use(cookieParser());

  // Added Helmet for Security
  expressInstance.use(helmet());

  // Configure Session properties
  expressInstance.use(session({
    secret: 'TEST_SECRET',
    cookie: { maxAge: 60000 },
    saveUninitialized: true,
    resave: true,
  }));

  // CSRF Protected. Must be after session and cookieParser
  expressInstance.use(csrf());

  // Create Repository Instance
  const repository = new Repository();

  // Add Flash connect
  expressInstance.use(flash());

  // Add Strategy to passport
  passport.use(appLocalStrategy(repository));
  appPassportConfig(repository, passport);

  // Required for authentication
  expressInstance.use(passport.initialize());
  expressInstance.use(passport.session());

  // Global app helper
  expressInstance.use(appHelper);

  // Global to detect user
  expressInstance.use(userInViews);

  // Add Middleware for Passport
  expressInstance.use(passportFlash);

  // Configure
  staticRoutesConfig(expressInstance, baseDirectory);

  // Set View Engine
  expressInstance.set('views', path.join(baseDirectory, 'src', 'views'));
  expressInstance.set('view engine', 'ejs');

  // Set the routes
  expressInstance.use('/', testRouter(passport));

  return expressInstance;
}

module.exports = (baseDirectory) => expressConfig(baseDirectory);
