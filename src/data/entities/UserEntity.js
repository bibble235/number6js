class UserEntity {
  constructor(id, username, password, email, firstname, lastname, knownAs) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.email = email;
    this.firstname = firstname;
    this.lastname = lastname;
    this.knownAs = knownAs;
  }
}

module.exports = UserEntity;
