module.exports = (req, res, next) => {
  res.locals.appName = process.env.npm_package_name;
  next();
};
