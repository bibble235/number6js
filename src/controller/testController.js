const { validationResult } = require('express-validator');

const debug = require('debug')('app:userController');

const ValidatorHelper = require('../validators/validatorHelper');

function userController(appPassport) {
  async function performHomeGet(req, res) {
    debug('Performing HomeGet');
    res.render('home', {
      csrf: req.csrfToken(),
      title: 'Home',
    });
  }

  async function performHomePost(req, res) {
    debug('Performing HomePost');
    return res.json('Home not Implemented');
  }

  async function performPublicGet(req, res) {
    debug('Performing PublicGet');

    res.render('public', {
      csrf: req.csrfToken(),
      title: 'Public',
    });
  }

  async function performPublicPost(req, res) {
    debug('Performing PublicPost');
    return res.json('Public not Implemented');
  }

  async function performLoginGet(req, res) {
    debug('Performing LoginGet');

    debug('here we are');
    debug(res.locals.passmessage);
    debug('here we are');

    res.render('login', {
      csrf: req.csrfToken(),
      title: 'Login',
      formValidation: {
        formWasValidated: '',
        usernameInvalid: '',
        usernameInvalidFeedbackText: '',
        passwordInvalid: '',
        passwordInvalidFeedbackText: '',
      },
    });
  }

  async function performLoginPost(req, res, next) {
    debug('Performing LoginPost');

    const formObject = {
      username: req.body.username,
      password: req.body.password,
    };

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      debug('Found Errors doing Login');

      const errorsArray = errors.array();

      res.render('login', {
        csrf: req.csrfToken(),
        title: 'Login',
        formValidation: {
          formWasValidated: 'was-validated',
          usernameInvalid: ValidatorHelper.getStateForParam(errorsArray, 'username'),
          usernameInvalidFeedbackText: ValidatorHelper.getFeedbackTextForParam(errorsArray, 'username'),
          passwordInvalid: ValidatorHelper.getStateForParam(errorsArray, 'password'),
          passwordInvalidFeedbackText: ValidatorHelper.getFeedbackTextForParam(errorsArray, 'password'),
        },
        formData: formObject,
      });
    } else {
      debug('We are calling the authenticate function');
      await appPassport.authenticate('local',
        {
          successRedirect: '/',
          failureRedirect: '/login',
          failureFlash: true,
        })(req, res, next);
    }
  }

  async function performProtectedGet(req, res) {
    debug('Performing ProtectedGet');
    res.render('protected', {
      csrf: req.csrfToken(),
      title: 'Protected',
    });
  }

  async function performProtectedPost(req, res) {
    debug('Performing ProtectedPost');
    return res.json('Protected not Implemented');
  }

  async function performLogoutGet(req, res) {
    req.logout();
    return res.redirect('/');
  }

  return {
    performHomeGet,
    performHomePost,
    performPublicGet,
    performPublicPost,
    performLoginGet,
    performLoginPost,
    performProtectedGet,
    performProtectedPost,
    performLogoutGet,
  };
}

module.exports = (appPassport) => userController(appPassport);
