const UserEntity = require('../entities/UserEntity');

const defaultId = 1;
const defaultUsername = 'fred.wiseman@bibble.co.nz';
const defaultPassword = '$2b$10$XStqWwQ33o43PmETEsxAPucl1NpGkpR5.5JMr1.RWED39ao3HeuOe';
const defaultFirstname = defaultUsername.substring(0, defaultUsername.indexOf('.'));
const defaultLastname = defaultUsername.substring(
  defaultUsername.indexOf('.') + 1,
  defaultUsername.indexOf('@'),
);

const defaultUser = new UserEntity(
  defaultId,
  defaultUsername,
  defaultPassword,
  defaultUsername,
  defaultFirstname,
  defaultLastname,
  `${defaultFirstname} ${defaultLastname}`,
);

class Repository {
  // eslint-disable-next-line class-methods-use-this
  findUserByUsername(query, done) {
    const { username } = query;
    if (username !== defaultUser.username) return done(null, false, { message: 'User not found.' });
    const user = defaultUser;
    return done(null, user);
  }

  // eslint-disable-next-line class-methods-use-this
  findUserById(userId, done) {
    if (userId !== defaultUser.id) return done(null, false, { message: 'User not found.' });
    const user = defaultUser;
    return done(null, user);
  }
}

module.exports = Repository;
