const express = require('express');

const path = require('path');

function staticRoutesConfig(expressInstance, baseDirectory) {
  // Set Static Paths
  expressInstance.use(express.static(path.join(baseDirectory, '/public/')));
  expressInstance.use('/css', express.static(path.join(baseDirectory, '/node_modules/bootstrap/dist/css')));
  expressInstance.use('/js', express.static(path.join(baseDirectory, '/node_modules/bootstrap/dist/js')));
  expressInstance.use('/js', express.static(path.join(baseDirectory, '/node_modules/jquery/dist')));
  expressInstance.use('/js', express.static(path.join(baseDirectory, '/node_modules/@popperjs/core/dist/umd')));
}

module.exports = staticRoutesConfig;
